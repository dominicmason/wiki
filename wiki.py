from elasticsearch import Elasticsearch
import pandas as pd
import urllib

es = Elasticsearch()


def make_dict(row):
    try:
        return {"title": urllib.parse.unquote(row["title"]),
                "head": urllib.parse.unquote(row["head"])}
    except TypeError:
        return None


def run():
    df = pd.read_csv("cleanWikiHead.csv", error_bad_lines=False)
    rows = len(df.index)
    bad = 0
    good = 0
    for i, row in df.iterrows():
        item = make_dict(row)
        if item:
            es.index(index="wiki", doc_type="title-head", id=i, body=item)
            good += 1
        else:
            bad += 1
        if i % 500 == 0:
            print("Success: {:.0%}, {:.0%} Complete, {} Good items".format(good / (bad + good), i/rows, good))
    return bad, good


errs, succ = run()

print("\n\nDone!\n\n")
print("Errors: {} Successes: {} ({:.0%})".format(errs, succ, succ/(errs+succ)))
